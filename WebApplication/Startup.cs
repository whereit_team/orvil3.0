using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Reflection;

namespace WebApplication
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                    //.AddFeatureFolders()
                    //.AddAreaFeatureFolders()
                    .AddApplicationPart(Assembly.Load(new AssemblyName("Context"))).AddControllersAsServices()
                    .AddApplicationPart(Assembly.Load(new AssemblyName("WebApi"))).AddControllersAsServices()
                    .AddApplicationPart(Assembly.Load(new AssemblyName("Rules"))).AddControllersAsServices();
            
            //services.AddMvc()

            //http://localhost:50908/Administration/SubFolder
            //http://localhost:50908/Administration/Test
            //http://localhost:50908/Bar/Home
            //http://localhost:50908/SubFoo/SubFoo

            // "Features" is the default feature folder root. To override, pass along 
            // a new FeatureFolderOptions object with a different FeatureFolderName
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseDeveloperExceptionPage();
            app.UseMvcWithDefaultRoute().UseMvc(routes =>
                routes.MapRoute(
                    name: "areaRoute",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}"));
        }
    }
}
