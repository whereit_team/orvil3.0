﻿using Microsoft.AspNetCore.Mvc;

namespace WebApplication.Generic
{
    public class GenericController : Controller
    {
        public IActionResult Home()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Search()
        {
            // do some stuff...
            return View("Home");
            // this will also work
            // return Redirect("/Foo/Home");

            // TODO: posting to /Foo/Home or /Bar/Home with Content-Type: application/x-www-form-urlencoded always routes to the "Bar" feature controller            
            // return RedirectToAction("Home");
        }
    }
}
