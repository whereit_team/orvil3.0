﻿using Models;

namespace RuleEngine.Livro
{
    [RuleEngine("Livro", Action.Insert | Action.Update)]
    public class RegraDeBloquear
    {
        public static bool Do(object obj)
        {
            // do something
            if (obj is Models.Livro)
            {
                return false;
            }
            return false;
        }
    }
}
