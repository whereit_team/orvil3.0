﻿namespace RuleEngine.Nota
{
    [RuleEngine("Nota", Action.Delete)]
    public class RegraConteudoErrado
    {
        public static object Do(object obj)
        {
            // do something
            if (obj is Models.Nota)
            {
                return true;
            }
            return false;
        }
    }
}
