﻿using System;
using System.Linq;
using System.Reflection;

namespace Context
{
    [System.AttributeUsage(System.AttributeTargets.Class | System.AttributeTargets.Struct)]
    public class REngine : Attribute
    {
        public String Class { get; set; }
        public Action Action { get; set; }

        public REngine(String Class_, Action Action)
        {
            this.Class = Class_;
            this.Action = Action;
        }

        public bool Magic(object obj, String Class, Action Action)
        {
            return GetClasses(Class, obj, Action);
        }

        private Type[] GetTypesInNamespace(Assembly assembly)
        {
            return assembly.GetTypes().ToArray();
        }

        private bool GetClasses(String NameSpace, object obj, Action Action)
        {
            var result = true;

            var typelist = GetTypesInNamespace(Assembly.GetExecutingAssembly()).ToList(); //.Where(w => w.Namespace.Split(".").LastOrDefault() == NameSpace).ToList();
            foreach (var i in typelist)
            {
                var type = Type.GetType(i.FullName + ", RuleEngine", true);
                var attrs = System.Attribute.GetCustomAttributes(type).FirstOrDefault();
                
                if (attrs is REngine)
                {
                    if (((Action) & Action) != 0)
                    {
                        result = (bool)Type.GetType(i.FullName + ", RuleEngine", true).GetMethod("Do").Invoke(null, new[] { obj });
                    }
                }
            }
            return result;
        }
    }

    [Flags]
    public enum Action
    {
        Insert,
        Update,
        Delete,
        All = Insert | Update | Delete
    }
}
