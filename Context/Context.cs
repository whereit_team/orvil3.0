﻿using Models;
using Microsoft.EntityFrameworkCore;

namespace Context
{
    public partial class Context : DbContext
    {
        public virtual DbSet<Livro> Livro { get; set; }
        public virtual DbSet<LivroUsuario> LivroUsuario { get; set; }
        public virtual DbSet<Nota> Nota { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder
                    //.UseLazyLoadingProxies()
                    .UseSqlServer("Server=orvildb.cf6z8717s8jf.us-east-1.rds.amazonaws.com,1433; User Id=whereit;Password=2pnnrv23;Database=Orvil");
            }
        }
    }
}
