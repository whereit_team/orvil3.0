﻿
using Context.Repository;
using Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Testes
{
    public class Class1
    {

        static void Main(string[] args)
        {
            var db = new RepositoryBase<Nota>();

            var lista = db.Get().ToList();
            var teste = "";
        }
    }

    public class Pais
    {
        public string Nome { get; set; }
    }

    public class Filhos
    {
        public string Nome { get; set; }
        public Pais Pais { get; set; }
    }

    public class Animal
    {
        public string Id { get; set; }
        public string Nome { get; set; }
        public Filhos Filhos { get; set; }
    }
}
