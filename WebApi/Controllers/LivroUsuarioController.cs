﻿using Context.Repository;
using Microsoft.AspNetCore.Mvc;
using Models;
using System;
using System.Linq;
using System.Transactions;

namespace WebApi.Controllers
{
    public class LivroUsuarioController : GenericController<LivroUsuario>
    {
        private readonly RepositoryBase<LivroUsuario> _repo;
        private readonly RepositoryBase<Nota> _repoNota;

        public LivroUsuarioController()
        {
            _repo = new RepositoryBase<LivroUsuario>();
            _repoNota = new RepositoryBase<Nota>();
        }

        [HttpDelete]
        public override IActionResult Delete(Int64 id)
        {
            var ent = _repo.Get(e => e.Id == id).FirstOrDefault();

            var notas = _repoNota.Get(e => e.IdLivro == ent.IdLivro && e.IdUsuario == ent.IdUsuario).ToList();

            if (ent == null) throw new Exception("Entidade não encontrada");

            //using (TransactionScope tran = new TransactionScope())
            //{

                foreach (var i in notas)
                {
                    _repoNota.Delete(Convert.ToInt32(i.Id));
                }

                _repo.Delete(Convert.ToInt32(ent.Id));
                _repo.Save();
                _repoNota.Save();
            //    tran.Complete();
            //}

            return Ok();
        }
    }
}
