﻿using Microsoft.AspNetCore.Mvc;
using Models;
using System.Collections.Generic;

namespace WebApi.Controllers
{
    public class LivroController : GenericController<Livro>
    {
        [HttpGet]
        [Route("teste2")]
        public ActionResult<IEnumerable<string>> teste2()
        {
            return new string[] { "value1", "8888" };
        }
    }
}
