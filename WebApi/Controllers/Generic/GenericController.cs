﻿using System;
using System.Collections.Generic;
using Context.Repository;
using DynamicQueryBuilder;
using DynamicQueryBuilder.Models;
using Microsoft.AspNetCore.Mvc;
using Rules;
using WebApi.Controllers.Generic;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GenericController<TEntity> : ControllerBase, IBaseController<TEntity> where TEntity : class
    {
        private readonly RepositoryBase<TEntity> _repo;

        public GenericController()
        {
            _repo = new RepositoryBase<TEntity>();
        }

        [HttpGet("{filter?}")]
        public virtual IEnumerable<TEntity> Get(String filter)
        {
            return String.IsNullOrEmpty(filter) ? _repo.Get() : _repo.Get().ApplyFilters((DynamicQueryOptions)JSON<DynamicQueryOptions>.Deserialize(filter));
        }

        [HttpPost]
        public virtual IActionResult Post(TEntity entity)
        {
            _repo.Add(entity);
            _repo.Save();

            return Ok();
        }

        [HttpPut]
        public virtual IActionResult Update(TEntity entity)
        {
            _repo.Update(entity);
            _repo.Save();

            return Ok();
        }

        [HttpDelete]
        public virtual IActionResult Delete(Int64 id)
        {
            _repo.Delete(id);
            _repo.Save();

            return Ok();
        }

        [HttpGet]
        [Route("{class_}/{args}")]
        public object CustomAction(String class_, String args)
        {
            return Type.GetType("Rules." + typeof(TEntity).Name + "." + class_ + ", Rules", true).GetMethod("Do").Invoke(null, new[] { args });
        }

        [HttpPost]
        [Route("{class_}")]
        public object CustomPost(String class_, [FromBody] String args)
        {
            return Type.GetType("Rules." + typeof(TEntity).Name + "." + class_ + ", Rules", true).GetMethod("Do").Invoke(null, new[] { args });
        }
    }
}
