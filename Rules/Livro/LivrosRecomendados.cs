﻿using Context.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rules.Livro
{
    public class LivrosRecomendados
    {
        public Int64 IdUsuario { get; set; }

        public static object Do(String args)
        {
            var param = (LivrosRecomendados)JSON<LivrosRecomendados>.Deserialize(args);

            var rep = new RepositoryBase<Models.LivroUsuario>();

            
            var livrosUsers = rep.Get(w => w.IdUsuario == param.IdUsuario).ToList();
            if (livrosUsers.Count > 0)
            {
                var data = livrosUsers.Select(s => s.Livro.Categorias + " " + s.Livro.Autor).Aggregate((s1, s2) => s1 + ", " + s2);

                return GetByGoogle.Do("{'Skip': 0, 'Take': 10, 'Value': '" + data + "'}");
            }
            else
            {
                return GetByGoogle.Do("{'Skip': 0, 'Take': 10, 'Value': '" + "livros mais lidos" + "'}");
            }
        }
    }
}
