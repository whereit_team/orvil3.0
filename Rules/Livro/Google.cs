﻿using Rules.Helpers;
using System;

namespace Rules.Livro
{
    public class Google
    {
        public String Base64Image { get; set; }

        public static object Do(String args)
        {
            var param = (Google)JSON<Google>.Deserialize(args);

            try
            {
                if (String.IsNullOrEmpty(param.Base64Image))
                    throw new Exception("Parâmetro 'Base64Image' é obrigatório.");

                var ocrApi = new GoogleOcr();

                return (ocrApi.Run(param.Base64Image, true));
            }
            catch (Exception e)
            {
                return (e.Message);
            }
        }
    }
}
