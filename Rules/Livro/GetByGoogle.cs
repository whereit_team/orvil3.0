﻿using Google.Apis.Services;
using Google.Apis.Books.v1;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Rules.Livro
{
    public class GetByGoogle
    {
        public String Value { get; set; }
        public long Take { get; set; }
        public long Skip { get; set; }

        public static object Do(String args)
        {
            var param = (GetByGoogle)JSON<GetByGoogle>.Deserialize(args);
            var lista = new List<Models.Livro>();
            var listaGoogle = new List<Models.Livro>();
            
            var service = new BooksService(new BaseClientService.Initializer
            {
                ApplicationName = "TCC Library Project",
                ApiKey = "AIzaSyD99U9v4aNLPWOM1X_2kscw6dgzy6rX1kA",
            });

            var action = service.Volumes.List(param.Value);
            action.MaxResults = param.Take;
            action.StartIndex = param.Skip;

            var result = action.Execute();

            if (result.Items == null)
            {
                throw new Exception("Livro não localizado.");
            }

            foreach (var item in result.Items)
            {
                var book = item.VolumeInfo;

                var livro = new Models.Livro()
                {
                    Titulo = book.Title,
                    Subtitulo = (book.Subtitle != null ? book.Subtitle : ""),
                    Autor = (book.Authors != null ? string.Join(",", book.Authors) : ""),
                    Descricao = (book.Description != null ? book.Description : ""),
                    Editora = (book.Publisher != null ? book.Publisher : ""),
                    NumeroPaginas = (book.PageCount != null ? Convert.ToInt64(book.PageCount) : 0),
                    Categorias = (book.Categories != null ? string.Join(",", book.Categories) : ""),
                    Idioma = (book.Language != null ? book.Language : ""),
                    AnoPublicacao = (String.IsNullOrEmpty(book.PublishedDate) ? "" : (book.PublishedDate.Length > 4 ? book.PublishedDate.Substring(0, 4) : book.PublishedDate))
                };

                if (book.ImageLinks != null)
                {
                    livro.SmallImage = book.ImageLinks.Small == null ? "" : book.ImageLinks.Small;
                    livro.MediumImage = book.ImageLinks.Medium == null ? "" : book.ImageLinks.Medium;
                    livro.LargeImage = book.ImageLinks.Large == null ? "" : book.ImageLinks.Large;
                    livro.ExtraLargeImage = book.ImageLinks.ExtraLarge == null ? "" : book.ImageLinks.ExtraLarge;
                    livro.SmallThumbnail = book.ImageLinks.SmallThumbnail == null ? "" : book.ImageLinks.SmallThumbnail;
                    livro.Thumbnail = book.ImageLinks.Thumbnail == null ? "" : book.ImageLinks.Thumbnail;
                }

                if (book.IndustryIdentifiers != null)
                    livro.Isbn = string.Join(",", book.IndustryIdentifiers.Select(s => s.Identifier));

                listaGoogle.Add(livro);
            }

            return lista.Union(listaGoogle.AsEnumerable().OrderBy(s => s.Id));   
        }
    }
}
