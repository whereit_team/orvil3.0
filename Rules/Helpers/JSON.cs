﻿using Nancy.Json;
using System;

namespace Rules
{
    public class JSON <T>
    {
        public static object Deserialize(String args)
        {
            JavaScriptSerializer oJS = new JavaScriptSerializer();
            return (T) oJS.Deserialize<T>(args);
        }
    }
}
