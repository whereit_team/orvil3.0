﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;

namespace Rules.Helpers
{
    public class GoogleObject
    {
        public String Text { get; set; }
        public String Locale { get; set; }
    }

    public class GoogleOcr
    {
        const String TokenGoogleApi = "AIzaSyD99U9v4aNLPWOM1X_2kscw6dgzy6rX1kA"; //"AIzaSyCE7Ypu2ByjWw2ihKMZ0FWGPh7q6bN3JDg";
        const String ContentType = "application/json";
        const String Type = "POST";
        const String UrlApi = "https://vision.googleapis.com/v1/images:annotate?key=";

        public GoogleObject Run(String image, Boolean isBase64)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(String.Format("{0}{1}", UrlApi, TokenGoogleApi));
            httpWebRequest.ContentType = ContentType;
            httpWebRequest.Method = Type;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = MakeJsonData(image, isBase64);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                dynamic obj = JsonConvert.DeserializeObject<dynamic>(streamReader.ReadToEnd());
                return new GoogleObject()
                {
                    Text = obj.responses[0].textAnnotations[0].description,
                    Locale = obj.responses[0].textAnnotations[0].locale
                };
            }
        }

        public String MakeJsonData(String image, Boolean isBase64)
        {
            if (isBase64)
                return "{ \"requests\": [ { \"image\": { \"content\": \" " + image + " \" }, \"features\": [ { \"type\": \"TEXT_DETECTION\" } ] } ] }";
            else
                return "{ \"requests\": [ { \"image\": { \"source\": { \"imageUri\": \"" + image + "\" } }, \"features\": [ { \"type\": \"TEXT_DETECTION\" } ] } ] }";
        }
    }
}
