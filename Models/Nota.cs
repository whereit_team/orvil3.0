﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models
{
    [Table(name: "nota")]
    public class Nota
    {
        public Nota()
        {
        }

        private ILazyLoader LazyLoader { get; set; }
        private Livro _livro;
        private Usuario _usuario;
        private Nota(ILazyLoader lazyLoader)
        {
            LazyLoader = lazyLoader;
        }

        [Key]
        [Column("id_notas")]
        [Required]
        public virtual Int64 Id { get; set; }

        [ForeignKey("Livro")]
        [Column("id_livro")]
        [Required]
        public virtual Int64 IdLivro { get; set; }
        public virtual Livro Livro {
            get => LazyLoader.Load(this, ref _livro);
            set => _livro = value;
        }

        [ForeignKey("Usuario")]
        [Column("id_usuario")]
        [Required]
        public virtual Int64 IdUsuario { get; set; }
        public virtual Usuario Usuario {
            get => LazyLoader.Load(this, ref _usuario);
            set => _usuario = value;
        }

        [Column("titulo")]
        [Required]
        public virtual String Titulo { get; set; }

        [Column("cor")]
        [Required]
        public virtual String Cor { get; set; }

        [Column("texto")]
        [Required]
        public virtual String Texto { get; set; }

        [Column("pagina")]
        [Required]
        public virtual Int64 Pagina { get; set; }

        [Column("tag")]
        [StringLength(50)]
        public virtual String Tag { get; set; }

        //[Column("data_criacao")]
        //public virtual DateTime DataCriacao { get; set; }
    }
}
