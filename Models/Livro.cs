﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models
{
    [Table(name: "livro")]
    public class Livro
    {

        public Livro() { }

        private ILazyLoader LazyLoader { get; set; }
        private ICollection<Nota> _notas;

        private Livro(ILazyLoader lazyLoader)
        {
            LazyLoader = lazyLoader;
        }

        [Key]
        [Column("id_livro", Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public virtual Int64 Id { get; set; }

        [Column("titulo")]
        [Required, StringLength(250)]
        public virtual String Titulo { get; set; }

        [Column("subtitulo")]
        [StringLength(250)]
        public virtual String Subtitulo { get; set; }

        [Column("autor")]
        [Required]
        public virtual String Autor { get; set; }

        [Column("descricao")]
        public virtual String Descricao { get; set; }

        [Column("editora")]
        [StringLength(250)]
        public virtual String Editora { get; set; }

        [Column("isbn")]
        public virtual String Isbn { get; set; }

        [Column("numero_paginas")]
        public virtual Int64 NumeroPaginas { get; set; }

        [Column("ano_publicacao")]
        public virtual String AnoPublicacao { get; set; }

        [Column("categorias")]
        public virtual String Categorias { get; set; }

        [Column("idioma")]
        [StringLength(50)]
        public virtual String Idioma { get; set; }

        [Column("smallimage")]
        public virtual String SmallImage { get; set; }

        [Column("mediumimage")]
        public virtual String MediumImage { get; set; }

        [Column("largeimage")]
        public virtual String LargeImage { get; set; }

        [Column("extralargeimage")]
        public virtual String ExtraLargeImage { get; set; }

        [Column("smallthumbnail")]
        public virtual String SmallThumbnail { get; set; }

        [Column("thumbnail")]
        public virtual String Thumbnail { get; set; }

        //[Column("data_criacao")]
        //public virtual DateTimeOffset DataCriacao { get; set; }
    }
}
