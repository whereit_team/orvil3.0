﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models
{
    [Table(name: "livro_usuario")]
    public class LivroUsuario
    {
        public LivroUsuario()
        {
        }

        private ILazyLoader LazyLoader { get; set; }
        private Livro _livro;
        private LivroUsuario(ILazyLoader lazyLoader)
        {
            LazyLoader = lazyLoader;
        }

        [Key]
        [Column("id_livroUsuario")]
        [Required]
        public virtual Int64 Id { get; set; }

        [ForeignKey("Livro")]
        [Column("id_livro")]
        [Required]
        public virtual Int64 IdLivro { get; set; }
        public virtual Livro Livro
        {
            get => LazyLoader.Load(this, ref _livro);
            set => _livro = value;
        }

        [Column("id_usuario")]
        [Required]
        public virtual Int64 IdUsuario { get; set; }

        //[Column("data_criacao")]
        //public virtual DateTime DataCriacao { get; set; }

        [Column("status_leitura")]
        [StringLength(10)]
        public virtual string StatusLeitura { get; set; }
    }
}
